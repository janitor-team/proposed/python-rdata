from ._conversion import (RExpression, RLanguage,
                          convert_list, convert_attrs, convert_vector,
                          convert_char, convert_symbol, convert_array,
                          Converter, SimpleConverter,
                          dataframe_constructor,
                          factor_constructor,
                          ts_constructor,
                          DEFAULT_CLASS_MAP, convert)
