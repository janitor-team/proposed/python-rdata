Source: python-rdata
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-setuptools,
               python3-all,
               python3-pytest-runner <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-numpy <!nocheck>,
               python3-xarray <!nocheck>
Standards-Version: 4.6.0
Homepage: https://github.com/vnmabus/rdata
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-rdata
Vcs-Git: https://salsa.debian.org/python-team/packages/python-rdata.git
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-rdata
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-numpy,
         python3-xarray,
         python3-pandas
Description: parses R dataset (.rda) files
 The common way of reading an R dataset consists on two steps:
 . 
  * First, the file is parsed using the function parse_file. This provides
    a literal description of the file contents as a hierarchy of Python
    objects representing the basic R objects. This step is unambiguous
    and always the same.
  * Then, each object must be converted to an appropriate Python
    object. In this step there are several choices on which Python type
    is the most appropriate as the conversion for a given R object. Thus,
    we provide a default convert routine, which tries to select Python
    objects that preserve most information of the original R object. For
    custom R classes, it is also possible to specify conversion routines
    to Python objects.
 .
 The basic convert routine only constructs a SimpleConverter objects and
 calls its convert method. All arguments of convert are directly passed
 to the SimpleConverter initialization method.
 .
 It is possible, although not trivial, to make a custom Converter object
 to change the way in which the basic R objects are transformed to
 Python objects. However, a more common situation is that one does not
 want to change how basic R objects are converted, but instead wants
 to provide conversions for specific R classes. This can be done by
 passing a dictionary to the SimpleConverter initialization method,
 containing as keys the names of R classes and as values, callables
 that convert a R object of that class to a Python object. By default,
 the dictionary used is DEFAULT_CLASS_MAP, which can convert commonly
 used R classes such as data.frame and factor.
